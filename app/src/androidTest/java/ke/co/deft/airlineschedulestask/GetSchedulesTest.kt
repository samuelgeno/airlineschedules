package ke.co.deft.airlineschedulestask

import android.app.PendingIntent.getActivity
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onData
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.typeText

import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.RootMatchers
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.util.Log
import ke.co.deft.airlineschedulestask.model.Airport
import org.hamcrest.CoreMatchers.*


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class GetSchedulesTest {

    @Rule
    @JvmField val activity =
        ActivityTestRule<MainActivity>(MainActivity::class.java)
    @Test
    @Throws(Exception::class)
    fun testSelectionOfAirports() {

        onView(withId(R.id.originAirportTextView)).
            perform(typeText("Kenyatt"))

        onData(instanceOf(Airport::class.java)).inRoot(RootMatchers.withDecorView(not(activity.getActivity().getWindow().getDecorView()))).perform(ViewActions.click())

        onView(withId(R.id.originAirportTextView)).
            perform(typeText(""))

        onView(withId(R.id.destinationAirportTextView)).
            perform(typeText("Kennedy"))

        onData(instanceOf(Airport::class.java)).inRoot(RootMatchers.withDecorView(not(activity.getActivity().getWindow().getDecorView()))).perform(ViewActions.click())


        var mainActivity = activity.activity as MainActivity
        var originAirportCode = mainActivity.originAirport?.code
        var destinationAirportCode = mainActivity.destinationAirport?.code

        check(mainActivity.originAirport != null && mainActivity.destinationAirport != null)
        check(originAirportCode.equals("NBO") && destinationAirportCode.equals("JFK"))

    }
}
