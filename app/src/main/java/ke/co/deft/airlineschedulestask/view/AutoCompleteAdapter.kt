package ke.co.deft.airlineschedulestask.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import ke.co.deft.airlineschedulestask.R
import ke.co.deft.airlineschedulestask.model.Airport

class AutoCompleteAdapter(private val mContext: Context, private var resultList: MutableList<Airport>) : BaseAdapter(),
    Filterable {

    private var postsList = resultList

    override fun getCount(): Int {
        return resultList.size
    }

    override fun getItem(index: Int): Airport {
        return resultList[index]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        if (convertView == null) {
            val inflater = mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        }
        (convertView!!.findViewById<View>(R.id.textView) as TextView).text = getItem(position).name

        return convertView
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                if (constraint != null) {

                    val PeoplesList = postsList.filter { it.name.contains(constraint.toString(), true) }

                    // Assign the data to the FilterResults
                    filterResults.values = PeoplesList
                    filterResults.count = PeoplesList.size
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results != null && results.count > 0) {
                    resultList = results.values as ArrayList<Airport>
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }
        }
    }

}