package ke.co.deft.airlineschedulestask.model

data class AirlineSchedule(val name:String,
                   val flights:List<Flight>)