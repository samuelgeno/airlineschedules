package ke.co.deft.airlineschedulestask.model

data class Flight(val departureAirport:String,
                           val departureTime:String,
                  val departureTerminal:String,
                  val arrivalAirport:String,
                  val arrivalTime:String,
                  val arrivalTerminal:String)