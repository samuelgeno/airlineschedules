package ke.co.deft.airlineschedulestask.model.remote

import com.google.gson.JsonElement
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

/**
 * Defines endpoints used to get token and schedules.
 */
interface WebService {
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("/v1/oauth/token")
    fun getToken(@Field("client_id") clientId: String,
                 @Field("client_secret") clientSecret: String,
                 @Field("grant_type") grantType: String): Deferred<Response<JsonElement>>


    @Headers("Accept: application/json")
    @GET("/v1/operations/schedules/{origin}/{destination}/{fromDateTime}")
    fun getSchedules(@Header("Authorization") authorization: String,
                     @Path("origin") origin: String,
                     @Path("destination") destination: String,
                     @Path("fromDateTime") fromDateTime: String): Deferred<Response<JsonElement>>
}