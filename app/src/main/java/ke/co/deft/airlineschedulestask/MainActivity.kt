package ke.co.deft.airlineschedulestask

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.maps.android.PolyUtil
import ke.co.deft.airlineschedulestask.model.Airport
import ke.co.deft.airlineschedulestask.view.AirlineSchedulesAdapter
import ke.co.deft.airlineschedulestask.view.AutoCompleteAdapter
import ke.co.deft.airlineschedulestask.viewmodel.AirlineSchedulesViewModel
import android.support.design.widget.Snackbar;
import ke.co.deft.airlineschedulestask.model.AirlineSchedule

import kotlinx.android.synthetic.main.activity_main.*
import java.util.ArrayList
import android.support.v7.widget.DividerItemDecoration



const val TAG = "airlineschedulestag"
const val FLIGHT_POLYLINE = "flight_polyline"

class MainActivity : AppCompatActivity() {

     var originAirport: Airport? = null
     var destinationAirport: Airport? = null

    private lateinit var airlineSchedulesViewModel: AirlineSchedulesViewModel

    private val airportsMap: MutableMap<String,Airport> = mutableMapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //load airports json file
        val jsonfile: String = applicationContext.assets.open("airports.json").bufferedReader().use {it.readText()}

        //parse json file into list of airports
        val listType = object : TypeToken<MutableList<Airport>>() { }.type
        val airportsList = Gson().fromJson<MutableList<Airport>>(jsonfile, listType)

        for(airport in airportsList){
        airportsMap.put(airport.code, airport)
        }
        // Initialize an adapter object for the origin airport list
        val originAirportAdapter = AutoCompleteAdapter(
            this, // Context
            airportsList// Array
        )


        // Set the AutoCompleteTextView adapter
        originAirportTextView.setAdapter(originAirportAdapter)


        // Auto complete threshold
        // The minimum number of characters to type to show the drop down
        originAirportTextView.threshold = 1


        // Set an item click listener for auto complete text view
        originAirportTextView.onItemClickListener = AdapterView.OnItemClickListener{
                parent,view,position,id->

            var item = parent.getItemAtPosition(position)
            if (item is Airport) {
                originAirportTextView.setText(item.name)
                originAirport = item
            }
        }

        // Initialize an adapter object for the destination airport list
        val destinationAirportAdapter = AutoCompleteAdapter(
            this, // Context
            airportsList// Array
        )


        // Set the AutoCompleteTextView adapter
        destinationAirportTextView.setAdapter(destinationAirportAdapter)


        // Auto complete threshold
        // The minimum number of characters to type to show the drop down
        destinationAirportTextView.threshold = 1


        // Set an item click listener for auto complete text view
        destinationAirportTextView.onItemClickListener = AdapterView.OnItemClickListener{
                parent,view,position,id->

            var item = parent.getItemAtPosition(position)
            if (item is Airport) {
                destinationAirportTextView.setText(item.name)
                destinationAirport = item
            }
        }

        airlineSchedulesViewModel = ViewModelProviders.of(this).get(AirlineSchedulesViewModel::class.java)

        val adapter = AirlineSchedulesAdapter(this)
        schedulesList.adapter = adapter
        schedulesList.layoutManager = LinearLayoutManager(this)

        val itemDecor = DividerItemDecoration(this, HORIZONTAL)
        schedulesList.addItemDecoration(itemDecor)

        airlineSchedulesViewModel.airlineSchedules.observe(this, Observer { airlineSchedules ->
            // Update the cached copy of the airline schedules in the adapter.
            airlineSchedules?.let { adapter.setAirlineSchedules(it) }
        })


        schedulesList.addOnItemClickListener(object: OnItemClickListener {
            override fun onItemClicked(position: Int, view: View) {
                val intent = Intent(this@MainActivity, MapActivity::class.java)
//  pass polyline to map activity
                val path = mutableListOf<LatLng>()

                airportsMap[adapter.airlineSchedules[position].flights[0].departureAirport]?.let {
                    path.add(LatLng(it.lat , it.lon))
                }

                for(flight in adapter.airlineSchedules[position].flights){
                    airportsMap[flight.arrivalAirport]?.let {
                        path.add(LatLng(it.lat , it.lon))

                        }
                }
                intent.putExtra(FLIGHT_POLYLINE, PolyUtil.encode(path.toList()))

// start map activity
                startActivity(intent)
                Log.d("onItemClicked: ", ""+position)
            }
        })

        airlineSchedulesViewModel.resultStatus.observe(this, Observer { resultStatus ->
            // hide progress and show error message
            Log.d("resultStatus ", "resultStatus changed "+resultStatus)
            resultStatus?.let {
                progress.visibility = View.GONE
                if(!it){
// Show a snack bar for undo option
                    Snackbar.make(
                        rootLayout, // Parent view
                        "Sorry, there was an error", // Message to show
                        Snackbar.LENGTH_LONG //
                    ).setAction( // Set an action for snack bar
                        "RETRY", // Action button text
                        { // Action button click listener
                            // Do something when action button clicked
                            getSchedules(null)
                        }).show() // Finally show the snack bar

                }
            }
        })
    }

    fun getSchedules(view: View?) {
        //reset any errors
        originAirportTextView.error = null
        destinationAirportTextView.error = null

        //if origin and destination selected get schedules
        if(originAirport != null && destinationAirport != null) {
            progress.visibility = View.VISIBLE
            //clear schedules
            airlineSchedulesViewModel.airlineSchedules.value = ArrayList<AirlineSchedule>()
            airlineSchedulesViewModel.getSchedules(originAirport!!.code, destinationAirport!!.code)
        }else{
            if(originAirport == null ) {
                originAirportTextView.error = getString(R.string.origin_error)
                originAirportTextView.requestFocus()
            }
            if(destinationAirport == null ) {
                destinationAirportTextView.error = getString(R.string.destination_error)
                //only request focus if origin is selected
                if(originAirport != null ) {
                    destinationAirportTextView.requestFocus()
                }
                }
        }
    }

    interface OnItemClickListener {
        fun onItemClicked(position: Int, view: View)
    }

    fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
        this.addOnChildAttachStateChangeListener(object: RecyclerView.OnChildAttachStateChangeListener {
            override fun onChildViewDetachedFromWindow(view: View?) {
                view?.setOnClickListener(null)
            }

            override fun onChildViewAttachedToWindow(view: View?) {
                view?.setOnClickListener({
                    val holder = getChildViewHolder(view)
                    onClickListener.onItemClicked(holder.adapterPosition, view)
                })
            }
        })
    }

    }
