package ke.co.deft.airlineschedulestask.model

data class Airport(val code:String,
                   val name:String,
                   val lat:Double,
                   val lon:Double)