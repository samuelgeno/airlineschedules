package ke.co.deft.airlineschedulestask

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.PolyUtil

class MapActivity : AppCompatActivity() , OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {

    private var googleMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.fragment_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
        this.googleMap?.setOnMapLoadedCallback(this)
        /* val path: MutableList<List<LatLng>> = ArrayList()
         val urlDirections = "https://maps.googleapis.com/maps/api/directions/json?origin=10.3181466,123.9029382&destination=10.311795,123.915864&key=AIzaSyCpX9A0QtG4d2DYi7C1aPoQAUG0bH8-id8"
         val directionsRequest = object : StringRequest(
             Request.Method.GET, urlDirections, Response.Listener<String> {
                     response ->
                 val jsonResponse = JSONObject(response)
                 // Get routes
                 val routes = jsonResponse.getJSONArray("routes")
                 val legs = routes.getJSONObject(0).getJSONArray("legs")
                 val steps = legs.getJSONObject(0).getJSONArray("steps")
                 for (i in 0 until steps.length()) {
                     val points = steps.getJSONObject(i).getJSONObject("polyline").getString("points")
                     path.add(PolyUtil.decode(points))
                 }
                 for (i in 0 until path.size) {
                     this.googleMap!!.addPolyline(PolylineOptions().addAll(path[i]).color(Color.RED))
                 }
             }, Response.ErrorListener {
                     _ ->
             }){}
         val requestQueue = Volley.newRequestQueue(this)
         requestQueue.add(directionsRequest)*/
    }

    override fun onMapLoaded() {

        var path = PolyUtil.decode(intent.getStringExtra(FLIGHT_POLYLINE))

        this.googleMap!!.addMarker(MarkerOptions().position(path[0]).title("Origin"))
        this.googleMap!!.addMarker(MarkerOptions().position(path[path.size-1]).title("Destination"))

        val latLongBounds = LatLngBounds.Builder()
        for(point in path)
        latLongBounds.include(point)

        this.googleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(latLongBounds.build(), 30))

        this.googleMap!!.addPolyline(PolylineOptions().addAll(path).color(Color.RED))

    }
}

