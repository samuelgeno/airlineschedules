package ke.co.deft.airlineschedulestask.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ke.co.deft.airlineschedulestask.R
import ke.co.deft.airlineschedulestask.model.AirlineSchedule

class AirlineSchedulesAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<AirlineSchedulesAdapter.AirlineScheduleViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    public var airlineSchedules = emptyList<AirlineSchedule>() //  copy of airline schedules

    inner class AirlineScheduleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val airlineScheduleItemView: TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AirlineScheduleViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return AirlineScheduleViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: AirlineScheduleViewHolder, position: Int) {
        val current = airlineSchedules[position]
        holder.airlineScheduleItemView.text = current.name
    }

    internal fun setAirlineSchedules(words: List<AirlineSchedule>) {
        this.airlineSchedules = words
        notifyDataSetChanged()
    }

    override fun getItemCount() = airlineSchedules.size
}