package ke.co.deft.airlineschedulestask.repository

import android.arch.lifecycle.MutableLiveData
import android.support.annotation.WorkerThread
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import ke.co.deft.airlineschedulestask.TAG
import ke.co.deft.airlineschedulestask.model.AirlineSchedule
import ke.co.deft.airlineschedulestask.model.Flight
import ke.co.deft.airlineschedulestask.model.remote.WebServiceFactory
import retrofit2.HttpException
import java.text.SimpleDateFormat
import java.util.*

class AirlineSchedulesRepository(val airlineSchedules: MutableLiveData<List<AirlineSchedule>>,
                                 val resultStatus: MutableLiveData<Boolean>) {

    private var token: String = ""

    /**
     * Get schedules from[origin] to [destination] from current date.
     */
    @WorkerThread
suspend fun getSchedules(origin: String, destination: String) {

    val service = WebServiceFactory.makeWebServiceService(WebServiceFactory.BASE_URL)

        try {
            if(token.isEmpty()) {
                val tokenRequest = service.getToken("4rvj8rkc4eppf3se59dkvpvv", "u3bEmqF3MJ", "client_credentials")

                val response = tokenRequest.await()
                response.body()?.let {

                    var tokenResponse = it as JsonObject
                    token = tokenResponse.get("access_token").asString

                    Log.d(TAG, "" + Gson().toJson(it.toString()))
                }
            }
                if(!token.isEmpty()){
                    val sdf = SimpleDateFormat("yyyy-MM-dd")
                    val currentDate = sdf.format(Date())
                    val request = service.getSchedules("Bearer $token", origin, destination, currentDate)

                    val response = request.await()
                    response.body()?.let {
                        var schedulesResponse = it as JsonObject
                        var schedulesArray = schedulesResponse.get("ScheduleResource").asJsonObject["Schedule"].asJsonArray
                        var schedulesList = ArrayList<AirlineSchedule>()
                        var scheduleJson: JsonObject
                        var flightsList = ArrayList<Flight>()
                        var flightArray: JsonArray
                        var flightJson: JsonObject

                        var airlineSchedule: AirlineSchedule
                        var flight: Flight
                        var scheduleName = ""
                        var departureTerminal : String
                        var arrivalTerminal: String

                        for(i in 0 until schedulesArray.size()){
                            scheduleJson = schedulesArray[i].asJsonObject
                            flightArray = scheduleJson["Flight"].asJsonArray
                            flightsList = ArrayList<Flight>()
                            scheduleName = ""
                            for(j in 0 until flightArray.size()){
                                flightJson = flightArray[j].asJsonObject

                                flight = Flight(flightJson["Departure"].asJsonObject["AirportCode"].asString,
                                    flightJson["Departure"].asJsonObject["ScheduledTimeLocal"].asJsonObject["DateTime"].asString,
                                    "",
                                    flightJson["Arrival"].asJsonObject["AirportCode"].asString,
                                    flightJson["Arrival"].asJsonObject["ScheduledTimeLocal"].asJsonObject["DateTime"].asString,
                                    "")

                                flightsList.add(flight)

                                scheduleName += if (j == 0) flight.departureAirport +"-"+flight.arrivalAirport else
                                    ("-"+flight.arrivalAirport+ if (j == flightArray.size() - 1) " | ${flight.departureTime}" else "-")

                            }
                            airlineSchedule = AirlineSchedule(scheduleName, flightsList)
                            schedulesList.add(airlineSchedule)
                        }
                        Log.d(TAG, "schedulesList: "+ schedulesList)

                        //post new values to update view
                        airlineSchedules.postValue(schedulesList)
                        resultStatus.postValue(true)

                        Log.d(TAG, ""+ Gson().toJson(it.toString()))
                    }
                }

        } catch (e: HttpException) {
            resultStatus.postValue(false)
            Log.d(TAG, e.message())
        } catch (e: Throwable) {
            resultStatus.postValue(false)
            Log.d(TAG, e.message)
        }
}

}