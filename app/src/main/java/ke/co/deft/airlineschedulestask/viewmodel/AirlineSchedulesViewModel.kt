package ke.co.deft.airlineschedulestask.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import ke.co.deft.airlineschedulestask.model.AirlineSchedule
import ke.co.deft.airlineschedulestask.repository.AirlineSchedulesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AirlineSchedulesViewModel(application: Application) : AndroidViewModel(application) {

    public var airlineSchedules: MutableLiveData<List<AirlineSchedule>> =MutableLiveData()
    public var resultStatus: MutableLiveData<Boolean> = MutableLiveData()

    init {
        resultStatus.value = true
    }

    private val repository: AirlineSchedulesRepository = AirlineSchedulesRepository(airlineSchedules,
        resultStatus)

    fun getSchedules(origin: String, destination: String) = GlobalScope.launch(Dispatchers.IO) {
        repository.getSchedules(origin, destination)
    }

    override fun onCleared() {
        super.onCleared()
    }
}