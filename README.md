# Airline Schedules App

This is a simple Android app that gets a list of airline schedules and displays their origins, stops and destinations on a map.

## Getting Started

### Clone the Repository

As usual, you get started by cloning the project to your local machine:

```
$ git clone https://gitlab.com/samuelgeno/airlineschedules.git
```

### Open and Run Project in Android Studio

Now that you have cloned the repo:

1. Open the project
 **File -> Open**
2. Go to the project folder and double click it or select and open it
3. Connect your Android device with debugging enabled
4. In Android Studio, click the app module in the Project window and then select  **Run -> Run**
5. In the **Select Deployment Target** window, select your device, and click OK

### Run Tests in Android Studio

1. Expand androidTest package then right click the test class
 `GetSchedulesTest` then choose ** Run GetSchedulesTest**

